# Buy your BOARD from anywhere (ESP32 or any other esp8266 & compatible boards family)

Be careful, this board is really expensive compared to arduino boards for example around 20 to 30$ 

Then Download the firmware from [micropython.org](http://micropython.org/download#esp32)

I currently use this board : [Sparkfun ESP32 thing](https://www.sparkfun.com/products/13907)

![pic](13907-01.jpg)


# get estptool python script
```
francois@zaphod:~/Downloads$ sudo pip install --upgrade esptool
Collecting esptool                       
  Downloading https://files.pythonhosted.org/packages/5c/85/5654e7b9019739d3d89af0adf528c9ae57a9a26682e3aa012e1e30f20674/esptool-2.6.tar.gz (80kB)
    100% |████████████████████████████████| 81kB 1.5MB/s
Collecting ecdsa (from esptool)
  Downloading https://files.pythonhosted.org/packages/63/f4/73669d51825516ce8c43b816c0a6b64cd6eb71d08b99820c00792cb42222/ecdsa-0.13-py2.py3-none-any.whl (86kB)
    100% |████████████████████████████████| 92kB 2.7MB/s
Collecting pyaes (from esptool)
  Downloading https://files.pythonhosted.org/packages/44/66/2c17bae31c906613795711fc78045c285048168919ace2220daa372c7d72/pyaes-1.6.1.tar.gz
Collecting pyserial>=3.0 (from esptool)
  Using cached https://files.pythonhosted.org/packages/0d/e4/2a744dd9e3be04a0c0907414e2a01a7c88bb3915cbe3c8cc06e209f59c30/pyserial-3.4-py2.py3-none-any.whl
Building wheels for collected packages: esptool, pyaes
  Running setup.py bdist_wheel for esptool ... done
  Stored in directory: /root/.cache/pip/wheels/cf/1f/62/7ad4e47843affd4f5b7032a39f1ef8a153c6d27533614d21aa
  Running setup.py bdist_wheel for pyaes ... done
  Stored in directory: /root/.cache/pip/wheels/bd/cf/7b/ced9e8f28c50ed666728e8ab178ffedeb9d06f6a10f85d6432
Successfully built esptool pyaes
Installing collected packages: ecdsa, pyaes, pyserial, esptool
  Found existing installation: pyserial 3.2.1
    Not uninstalling pyserial at /usr/lib/python2.7/dist-packages, outside environment /usr
Successfully installed ecdsa-0.13 esptool-2.6 pyaes-1.6.1 pyserial-3.4
francois@zaphod:~/Downloads$ which esptool.py
/usr/local/bin/esptool.py
francois@zaphod:~/Downloads$ 

```
# REMOVE ORIGINAL FIRMWARE
```
francois@zaphod:~/Downloads$ esptool.py --chip esp32 -p /dev/ttyUSB0 erase_flash
esptool.py v2.6
Serial port /dev/ttyUSB0
Connecting.......
Chip is ESP32D0WDQ6 (revision 1)
Features: WiFi, BT, Dual Core, 240MHz, VRef calibration in efuse, Coding Scheme None
MAC: 30:ae:a4:bb:f6:ac
Uploading stub...
Running stub...
Stub running...
Erasing flash (this may take a while)...
Chip erase completed successfully in 8.2s
Hard resetting via RTS pin...
francois@zaphod:~/Downloads$

```
# write Python firmware into ESP32 SPARKFUN BOARD
```
francois@zaphod:~/Downloads$ esptool.py --chip esp32 -p /dev/ttyUSB0 write_flash -z 0x1000 esp32-20190207-v1.10-54-g43a894fb4.bin
esptool.py v2.6
Serial port /dev/ttyUSB0
Connecting....
Chip is ESP32D0WDQ6 (revision 1)
Features: WiFi, BT, Dual Core, 240MHz, VRef calibration in efuse, Coding Scheme None
MAC: 30:ae:a4:bb:f6:ac
Uploading stub...
Running stub...
Stub running...
Configuring flash size...
Auto-detected Flash size: 4MB
Compressed 1130208 bytes to 712738...
Wrote 1130208 bytes (712738 compressed) at 0x00001000 in 63.3 seconds (effective 142.8 kbit/s)...
Hash of data verified.

Leaving...
Hard resetting via RTS pin...
francois@zaphod:~/Downloads$

```

![pic](20190307_221636.jpg)

# NOW GET FUN 
```
francois@zaphod:~/Downloads$ rshell
Welcome to rshell. Use Control-D (or the exit command) to exit rshell.

No MicroPython boards connected - use the connect command to add one

/home/francois/Downloads> connect serial /dev/ttyUSB0 115500
Connecting to /dev/ttyUSB0 ...
/home/francois/Downloads> repl
Entering REPL. Use Control-X to exit.
repl_serial_to_stdout dev = <rshell.main.DeviceSerial object at 0x7f82b9d1ada0>
>
MicroPython v1.10-54-g43a894fb4 on 2019-02-07; ESP32 module with ESP32
Type "help()" for more information.
>>>
>>> from machine import Pin
>>> led = Pin(5, Pin.OUT)
>>> led.value(0)
>>> led.value(1)
>>> led.value(0)
>>> led.value(1)
>>>
>>>
>>> # YEAH !
>>>
>>> autoconnect: /dev/ttyUSB0 action: remove

                                            USB Serial device '/dev/ttyUSB0' disconnected

serial port pyboard closed
/home/francois/Downloads>
/home/francois/Downloads>
francois@zaphod:~/Downloads$
```

# Now it's time for FUN 

soldering in with captors/motors drivers/LCD displays or whatever you want, & you can code it as python without boring compilation errors 

build your robots or  domestic tools or whatever you might imagine.


# TO DO SO I need a prototyping tool 

so I can run micro-python on my ESP32 from the PC with the USB connection to run already prepared .py files

Then only when I will be satisfied with my code, I will upload it as "boot" on the ROM.

the main tool to do that is ampy (from Ada-fruit) 

```
francois@zaphod:~/Downloads$ sudo pip install adafruit-ampy
[sudo] password for francois:
Collecting adafruit-ampy
  Downloading https://files.pythonhosted.org/packages/59/99/f8635577c9a11962ec43714b3fc3d4583070e8f292789b4683979c4abfec/adafruit_ampy-1.0.7-py2.py3-none-any.whl
Requirement already satisfied: click in /usr/lib/python2.7/dist-packages (from adafruit-ampy)
Collecting python-dotenv (from adafruit-ampy)
  Downloading https://files.pythonhosted.org/packages/8c/14/501508b016e7b1ad0eb91bba581e66ad9bfc7c66fcacbb580eaf9bc38458/python_dotenv-0.10.1-py2.py3-none-any.whl
Requirement already satisfied: pyserial in /usr/lib/python2.7/dist-packages (from adafruit-ampy)
Installing collected packages: python-dotenv, adafruit-ampy
Successfully installed adafruit-ampy-1.0.7 python-dotenv-0.10.1
francois@zaphod:~/Downloads$
```

# Other test : 

```
>>> import time
>>> from machine import Pin
>>> led=Pin(5,Pin.OUT)
>>> x=0
>>>
>>> while x < 10:
...     led.value(0)
...     time.sleep(0.5)
...     led.value(1)
...     time.sleep(0.250)
...     x=x+1
...
>>>
```

![pic](20190307_221558.jpg)

# other boards 
[video](https://youtu.be/0Shihs0c804)


## many other projects on the YouTube channel :

[youtube](https://www.youtube.com/channel/UCnsW_UH9vXX-nBe3X-enXYA)

you can also join mo by mail at: [goblinrieur@gmail.com](mailto://goblinrieur@gmail.com)

if you prefer to discuss on discord : [discord](https://discord.gg/9szvduB)
